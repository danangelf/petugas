package com.example.coba_uts

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_form_pendataan.*
import java.util.HashMap

class FormPendataanActivity : AppCompatActivity() {
    var pendataan : Pendataan? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form_pendataan)

        val data = intent.getSerializableExtra("pendataan")
        var edit = true

        val database = FirebaseDatabase.getInstance()
        val myRef = database.getReference("pendataan1")

        if (data != null) {
            pendataan = data as Pendataan
            etNikEdit.setText(pendataan?.nik)
            etNamaLengkapEdit.setText(pendataan?.nama)
            etAlamatEdit.setText(pendataan?.alamat)

            // btUpdateForm.setText("Edit")
        } else {
            btUpdateForm.setText("Tambah")
            edit = false
        }

        btUpdateForm.setOnClickListener {
            if (edit) {
                val changeData = HashMap<String, Any>()
                changeData.put("nik", etNikEdit.text.toString())
                changeData.put("nama", etNamaLengkapEdit.text.toString())
                changeData.put("alamat", etAlamatEdit.text.toString())

                myRef.child(pendataan?.key.toString()).updateChildren(changeData)
                finish()
                startActivity(Intent(this, DataPemilih::class.java))
            } else {
                val key = myRef.push().key

                val newNote = Pendataan()
                newNote.nik = etNikEdit.text.toString()
                newNote.nama = etNamaLengkapEdit.text.toString()
                newNote.alamat = etAlamatEdit.text.toString()

                myRef.child(key.toString()).setValue(newNote)
                finish()
                startActivity(Intent(this, DataPemilih::class.java))
            }
        }
    }
}
